# Flectra Community / l10n-portugal

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[l10n_pt_stock_invoicexpress](l10n_pt_stock_invoicexpress/) | 2.0.3.0.0| Portuguese legal transport and shipping documents (Guias de Transporte e Guias de Remessa) generated with InvoiceXpress
[l10n_pt_account_invoicexpress](l10n_pt_account_invoicexpress/) | 2.0.4.1.3| Portuguese certified invoices using InvoiceXpress
[l10n_pt_vat](l10n_pt_vat/) | 2.0.1.0.1| Portuguese VAT requirements extensions


